package com.mba.balance.model;


import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Builder

public class Balance {

    public String eventType;
    public String timestamp;
}
