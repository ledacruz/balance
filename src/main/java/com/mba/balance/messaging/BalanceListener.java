package com.mba.balance.messaging;

import com.google.gson.Gson;
import com.mba.balance.model.Balance;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class BalanceListener {

    @Autowired
    BalanceProducer balanceProducer;

    @Value("${topic-choreography}")
    private String choreographyTopic;

    @Value("${topic-out}")
    private String orchestratorTopic;

    private final Logger logger = LoggerFactory.getLogger(BalanceListener.class);

    private String producerMessage;

    @KafkaListener(topics = "${topic-choreography}", groupId = "group-balance")
    public void consumeChoreography(String message){
        logger.info(String.format("$$ -> Consumed Message -> %s",message));


        Balance balance = convertStringToBalance(message);

        switch (balance.getEventType()) {
            case "transferSender.sucess":
                producerMessage = "balance.sucess";
                //producerMessage = "balance.failure";
                callProducer(producerMessage, choreographyTopic);
                break;
            default:
                logger.info(String.format("$$ -> Consumed not a know Message -> %s",message));
        }
    }

    @KafkaListener(topics = "${topic-in}", groupId = "group-balance")
    public void consumeOrchestration(String message){
        logger.info(String.format("$$ -> Consumed Message -> %s",message));


        Balance balance = convertStringToBalance(message);

        switch (balance.getEventType()) {
            case "balance.requested":
                producerMessage = "balance.sucess";
                //producerMessage = "balance.failure";
                callProducer(producerMessage, orchestratorTopic);
                break;
        }
    }

    private void callProducer(String message, String topic){

        balanceProducer.sendMessage(producerMessage, topic);
    }

    private Balance convertStringToBalance(String message){
        Gson gson = new Gson();
        Balance balance = gson.fromJson(message, Balance.class);

        return balance;

    }
}
