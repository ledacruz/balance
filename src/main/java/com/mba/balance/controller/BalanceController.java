package com.mba.balance.controller;

import com.mba.balance.messaging.BalanceProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

    @RestController
    @RequestMapping(value = "/kafka")
    public class BalanceController {
        private final BalanceProducer balanceProducer;

        @Autowired
        public BalanceController(BalanceProducer balanceProducer) {
            this.balanceProducer = balanceProducer;
        }
        @PostMapping(value = "/publish")
        public void sendMessageToKafkaTopic(@RequestParam("message") String message){
            this.balanceProducer.sendMessage(message, "");
        }
    }